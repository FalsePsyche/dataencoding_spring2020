// DE11B.java CS5125/6025 Cheng 2020
// LZW decoding
// without handling the special situation when codeword == dictionarySize - 1
// see lecture note 4/8/2020
// java DE11B < encoded > original

import java.io.*;
import java.util.*;

public class DE11B{

 static final int dictionaryCapacity = 4096;
 int[] prefix = new int[dictionaryCapacity];
 int[] lastSymbol = new int[dictionaryCapacity];
 int dictionarySize = 0;
 int firstSymbol = 0;

 void initializeDictionary(){
   for (int i = 0; i < 256; i++){
      prefix[i] = -1; lastSymbol[i] = i;
   }
   dictionarySize = 256;
 }

 void outputPhrase(int index){  // recursive
   if (index >= 0 && index < dictionarySize){ 
     outputPhrase(prefix[index]);
     System.out.write(lastSymbol[index]);
     if (index < 256) firstSymbol = lastSymbol[index];
   }
 }  

 void decode(){
   Scanner in = new Scanner(System.in);
   while (in.hasNextLine()){
     int codeword = Integer.parseInt(in.nextLine());
     outputPhrase(codeword);
     if (dictionarySize < dictionaryCapacity){
         lastSymbol[dictionarySize - 1] = firstSymbol;
         prefix[dictionarySize++] = codeword;

    //    lastSymbol[dictionarySize - 1] = ?
    //    prefix[dictionarySize++] = ?
     }
   } 
 }  

 public static void main(String[] args){
   DE11B de11 = new DE11B();
   de11.initializeDictionary();
   de11.decode();
 }
}
